![s](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# Installation of Hadoop - Part 1

### Hands on Lab Exercise 1

Contents

1. Prerequisites and Downloads for Installing Hadoop


#### Prerequisites and Downloads for Installing Hadoop

In this Exercise, we will be using  **APACHE HADOOP ** for doing most of the hands-on labs. Apache Hadoop software is an open-source software that enables the distributed processing and efficient storage purpose of large datasets across multiple clusters of computers.

In this lab we will download and install the resources needed for Apache Hadoop.

**PREREQUISITES TO INSTALL HADOOP:**

# The Prerequisite for installing Hadoop are:

- Java 1.7

- Hadoop 2.X

# JAVA DOWNLOAD

Check before downloading it because mostly this version is pre-installed in every system.

Go to command prompt and type command **java -version**.

if not installed then follow the below steps.

**Step 1:** Click on [Download java](https://www.oracle.com/java/technologies/javase/javase7-archive-downloads.html)

Select you operating system version and click on **jdk** link.

![java download](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_1.7_version__2_.png)


**Step 2:** After clicking on jdk link you will get this option shown below, just accept it and select download option.

![oracle signup](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_download_oracle_signin.png)

**Step 3:** After clicking on download option you will be directed to oracle login page. Create your account and sign in.

As you sign in, java jdk file will start downloading.

![signup](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_oracle.png)

Note: We will be downloading, installing, and configuring Java and Apache Hadoop simultaneously to save our time.

# APACHE HADOOP DOWNLOAD

**Step 1:** Click on [Download hadoop](https://hadoop.apache.org/release/2.4.1.html) 

Select **Download tar.gz** file(green box) and hadoop tar file will be dowanloaded.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.png)

**NOTE:** if there is no option like WinRAR then download it from this link [WinRAR](https://www.win-rar.com/predownload.html?&L=0) 

![winrar](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/winrar.png)

**Step 2:** Extract the downloaded file using WinRAR.

when you click on extract file this extraction path and options page will open.

Select C:\ drive and make new folder named as “HADOOP” then click ok.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_extract.png)

You can see, hadoop file is extracting.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/extracted_hadoop.png)

**After the extraction is completed, you will get this diagnostic message, just close it**

**Step 3:** Then you will see Hadoop folder created, open it and you will find a folder named **“hadoop-2.4.1”**. Open this folder all Hadoop files are inside this folder. 

**“NOTE: Within the Hadoop folder you will find another folder hadoop-2.4.1 containing all the hadoop files. Copy all the hadoop files from hadoop-2.4.1 folder and paste it inside the main folder Hadoop so that we have all files inside the main folder. After copying delete hadoop-2.4.1 folder. Now the bin path will be C:\Hadoop\bin.”**

# JAVA INSTALLATION

**Step 1:** Click on downloaded **jdk** file and select “Next”

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_1.png)

**Step 2:** Click on **“Next”** and check it must be installed in C:\ drive only.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_2.png)

**Step 3:** Then Click **“Change”** and in C:\ drive, create new folder **“JAVA or similar name”** and click on “Next”.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_4.png)

**Step 4:** Java starts installing.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_5.png)

**Step 5:** Go to **“Program files"** in C:\ drive, you will find a folder named there as “java” open it and copy the jdk folder from there.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_6.png)

**Step 6:** Paste this jdk file in the folder created at installation time “JAVA’’ in C:\ drive. Open this JAVA folder and paste **jdk** file here.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_7.png)

**Step 7:** **NOTE:** Delete that java folder from program files in C:\ drive. There should be only one java file.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java.png)


**Your Java and Apache Hadoop has been successfully installed. Now it is time to setup the environment variables and configuration of Apache Hadoop, which we will be doing on the next lab.**


                                                                       **This Ends the Exercise.**
















































