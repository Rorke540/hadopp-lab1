# Installation of Hadoop - Part 1

### Hands on Lab Exercise 1

Contents

1. Prerequisites and Downloads for Installing Hadoop


#### Prerequisites and Downloads for Installing Hadoop

In this lab, we will be using  **APACHE HADOOP ** for doing most of the hands-on labs. Apache Hadoop software is an open-source software that enables the distributed processing and efficient storage purpose of large datasets across multiple clusters of computers.

**PREREQUISITES TO INSTALL HADOOP:**

**JAVA DOWNLOAD: &quot;** Hadoop supports only java 8&quot;

Step 1: Let us Install Java.

Click on https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html

![image](Images/1.jpg)

Step 2: Scroll down and you will get various versions of java 8 available for (Linux, macOS and windows). Select your version and click on the jdk links.

![image](Images/2.jpg)

Step 3: After clicking on jdk link you will get this option shown below, just accept it and select download option .

![image](Images/3.jpg)

Step 4: After clicking on download option you will be directed to oracle login page. Create your account and sign in.

![image](Images/4.jpg)

Step 5: As you sign in, you will see java 8 start downloading.

![image](Images/5.jpg)

Note: We will be downloading, installing, and configuring Java and Apache Hadoop simultaneously to save our time.

**APACHE HADOOP DOWNLOAD:**

Step 1:  Click on [https://hadoop.apache.org/releases.html](https://hadoop.apache.org/releases.html) Select 3.1.3 version and click on binary

![image](Images/6.jpg)

Step 2: Then you will be directed to this page shown below. Click on the marked link and Apache will start downloading.

![image](Images/7.jpg)

Both Java and Apache Hadoop are successfully downloaded now.

**Let&#39;s move to Java installation now:**

Transfer both downloaded file to your C:\ drive.

![image](Images/8.jpg)



**JAVA INSTALLATION:**

Step 1:  Click on downloaded jdk file and select &quot;Next&quot;.

![image](Images/9.jpg)

Step 2: Click on &quot;Next&quot; and check it must be installed in C drive only.

![image](Images/10.jpg)



Step 3: Then Click &quot;Change&quot;.

![image](Images/11.jpg)



Step 4: In C drive, create new folder &quot;JAVA&quot; and click on &quot;Next&quot;

![image](Images/12.jpg)



Step 5: Installation of Java Started.

![image](Images/13.jpg)

Step 6: Go to &quot;Program files &quot; in C drive, you will find a folder named there as &quot;java&quot; open it and copy the jdk folder from there.

![image](Images/14.jpg))



![image](Images/15.jpg))

Step 7: Paste this jdk file in the folder created at installation time &quot;JAVA&#39;&#39; in C drive. Open this JAVA folder and paste jdk file here.

![image](Images/16.jpg)



![image](Images/17.jpg)

NOTE: Delete that java folder from program files in C drive. There should be only one java file to eliminate error and confusion.

![image](Images/18.jpg)



Your Java has been successfully installed. Now it is time to setup the environment variables.


  ##This Ends the Exercise.