![s](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/skillup.png)

# Installation of Hadoop - Part 2

### Hands on Lab Exercise 2

Contents

1. Environmental Variables settings.
2. Configuration of Hadoop Files.

### Environmental variables setting for JAVA.

In this Lab we will set the environmental variables for JAVA and Apache HADOOP. After that we will Configure some file in Hadoop, to get the Apache Hadoop start running succesfully.

**First we will set the environmental variables for JAVA.**

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables.

Write variable name as: “JAVA_HOME” and variable value is the path location of jdk bin.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_8.png)

**Step 2:** How to get the path location of jdk bin.

Go to JAVA folder in C:\ drive --> open jdk1.7.0_80 file --> open bin. Then copy the path.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_9.png)

**Step 3:** Double Click on **path** in System Variables --> This opens path, click on **“New”** and then paste the path location of jdk bin here also. Then click **ok** on all open pages. Now you are done with setting up environment variables.

![java](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/java_10.png)


### Veryfying the Installation of JAVA.(COMMAND PROMPT)

**Step 1:** Open Command prompt and type **“javac”** and enter.

![cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/cmd_java.png)

**Step 2:** To know the version of java installed type **“java -version”**. It will appear like this.

![cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/cmd_java_1.png)



### Configuration of HADOOP Files:

To edit hadoop file you will need **Notepad++**.

Click on this link or copy paste the link on new tab.

https://notepad-plus.en.softonic.com/download

![notepad](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/notepad.png)

**Lets start with editing now.**

**Step 1:** Open **Hadoop** folder --> open **“etc”** folder --> Inside you will find **hadoop** folder, open it:

We are going to edit 5 files here.

1st file= **core-site** (XML DOCUMENT) right click on this file and select option **edit with notepad++**.

Page will open like this:

**NOTE: don’t forget to save all the files after you paste your values in it. Save it after editing forsure.**

![core-site XML](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-1.png)

Copy and paste this value given below inside the configuration in core-site.xml file.


    
    <property>

        <name>fs.defaultFS</name>

        <value>hdfs://localhost:9000</value>

    </property>


After pasting it, it will look like this.

![core-site XML](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-4.png)

In the same way edit hdfs-site, mapred-site, and yarn-site (XML document).

**NOTE: To assist you, we have given below the value for each file**


**Hdfs-site.xml**

Before editing this file, go to Hadoop folder in C:\ drive and create a new folder named as data.

Inside data folder create 2 folders **“namenode”** and **“datanode”**. And in value you must paste **path location** of your datanode and namenode.

![data folder](https://gitlab.com/Rorke540/hadopp-lab1/-/raw/master/Images/Image-2.png)

**Values are:**

**Hdfs-site.xml**

   

    <property>

        <name>dfs.replication</name>

        <value>1</value>

    </property>

    <property>

        <name>dfs.namenode.name.dir</name>

        <value><path of namednode></value>

    </property>

    <property>

        <name>dfs.datanode.data.dir</name>

        <value><path of datanode></value>

    </property>


**Mapred-site.xml**

    <property>

        <name>mapreduce.framework.name</name>

        <value>yarn</value>

    </property>

**Yarn-site.xml**
    
    <property>

        <name>yarn.nodemanager.aux-services</name>

        <value>mapreduce_shuffle</value>

    </property>

    <property>

        <name>yarn.nodemanager.auxservices.mapreduce.shuffle.class</name>

        <value>org.apache.hadoop.mapred.ShuffleHandler</value>

    </property>

**Now open Hadoop-env (window command file) and edit it with notepad++.**

![hadoop cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_cmd.png)

Edit here **JAVA_HOME= “variable value"** you entered when you were creating the environmental variable for JAVA”. Open your setting and see your variable value. 

**Note: When you enter the Variable Value kindly remove the "bin".**

**REMINDER: do not forget to save your edited files.**

### SETTING UP THE ENVIRONMENT VARIABLES FOR HADOOP:

**Step 1:** Click on start and go to **settings** --> In setting go to **“system”** and then search for **“edit for environment variables”** --> Select **“environment variables”** and click ok --> Click on **“New”** in User Variables section.

Write variable name as: **“HADOOP_HOME”** and variable value is the path location of bin.

![hadoophome](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_home.png)

**Step 2:** Then click on **“Path”** in System Variables, select **“New”** enter the variable value of Hadoop bin folder here as well.

![hadooppath](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop_path.png)

**Step 3:** Then add the path location of hadoop’s **sbin** folder as well and click ok.

![sbin](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/sbin.png)

**In Hadoop we are missing some configuration file, let’s fix that**

**Step 1:** Click on this link https://drive.google.com/file/d/1AMqV4F5ybPF4ab4CeK8B3AsjdGtQCdvy/view or copy paste this link on new tab.

**Step 2:** From download folder copy this file and paste it inside the Hadoop folder.

**Step 3:** Extract this Hadoop configuration file.

![configuration](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/configuration_file.png)

**Step 4:** A new folder named as **“HadoopConfiguration-FIXbin”** is created. Open it copy that bin folder and **replace** the previous bin. Now delete unnecessary files of Hadoop configuration.

### Veryfying the installation of HADOOP(COMMAND PROMPT)

**Step 1:** Open command prompt and type **“hdfs namenode -format”** and enter.

![hadoop.cmd](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.cmd.png)

**Step 2:** Type **start-all.cmd** and enter.

![hadoop](https://gitlab.com/Pallavi_rai/apache_pig/-/raw/master/images/hadoop.cmd_2.png)

**All files are opening now**

This brings us to the end of this lab. 


                                                                         **This Ends the Exercise.**

